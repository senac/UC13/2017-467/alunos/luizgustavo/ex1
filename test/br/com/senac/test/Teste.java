/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.test;

import exercicio.Pessoa;
import exercicio.Programa;

import static exercicio.Programa.getPessoaMaisNova;
import static exercicio.Programa.getPessoaMaisVelha;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sala304b
 */
public class Teste {
    
    public Teste() {
    }
    
    @Test
    public void gustavoMaisVelho(){
        Pessoa pessoaA = new Pessoa("Luiz", 20);
        Pessoa pessoaB = new Pessoa("Gustavo", 30);
        Pessoa pessoaC = new Pessoa("LG",10);
        
        
        List<Pessoa> lista = new ArrayList<>();
        lista.add(pessoaA);
        lista.add(pessoaB);
        lista.add(pessoaC);
        
        Pessoa pessoaMaisVelha = Programa.getPessoaMaisVelha(lista);
        
        assertEquals(pessoaB, pessoaMaisVelha);
    }
    @Test
    public void lgMaisNovo(){
        Pessoa pessoaA = new Pessoa("Luiz", 20);
        Pessoa pessoaB = new Pessoa("Gustavo", 30);
        Pessoa pessoaC = new Pessoa("LG",10);
        
        
        List<Pessoa> lista = new ArrayList<>();
        lista.add(pessoaA);
        lista.add(pessoaB);
        lista.add(pessoaC);
        
        Pessoa pessoaMaisNova = getPessoaMaisNova(lista);
        
        assertEquals(pessoaC, pessoaMaisNova);
    }
    
    @Test
    public void luizNaoMaisNovo(){
        Pessoa pessoaA = new Pessoa("Luiz", 20);
        Pessoa pessoaB = new Pessoa("Gustavo", 30);
        Pessoa pessoaC = new Pessoa("LG",10);
        
        
        List<Pessoa> lista = new ArrayList<>();
        lista.add(pessoaA);
        lista.add(pessoaB);
        lista.add(pessoaC);
        
        Pessoa menorIdade = getPessoaMaisNova(lista);
        
        assertNotEquals(pessoaA, menorIdade);
        
        
    }
    @Test
    public void luizNaoMaisVelho(){
         Pessoa pessoaA = new Pessoa("Luiz", 20);
        Pessoa pessoaB = new Pessoa("Gustavo", 30);
        Pessoa pessoaC = new Pessoa("LG",10);
        
        
        List<Pessoa> lista = new ArrayList<>();
        lista.add(pessoaA);
        lista.add(pessoaB);
        lista.add(pessoaC);
        
        Pessoa pessoaMaisVelha = getPessoaMaisVelha(lista);
        
        assertNotEquals(pessoaA, pessoaMaisVelha);
    }
    
}
