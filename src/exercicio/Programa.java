/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicio;

import java.util.ArrayList;
import java.util.List;


public class Programa {
    
  
    
    public static void main(String[] args) {
        Pessoa pessoaA = new Pessoa("Luiz", 20);
        Pessoa pessoaB = new Pessoa("Gustavo", 30);
        Pessoa pessoaC = new Pessoa("LG",10);
        
        
        List<Pessoa> lista = new ArrayList<>();
        lista.add(pessoaA);
        lista.add(pessoaB);
        lista.add(pessoaC);
        
        Pessoa pessoaMaisVelha = Programa.getPessoaMaisVelha(lista);
        Pessoa pessoaMaisNova = Programa.getPessoaMaisNova(lista);
        
        
        System.out.println("A pessoa mais nova é: " + getPessoaMaisNova(lista).getNome());
        System.out.println("A pessoa mais velha é: " + getPessoaMaisVelha(lista).getNome());
        
       
    }
    
    public static Pessoa getPessoaMaisVelha(List<Pessoa> lista){
       
        Pessoa pessoaMaisVelha = lista.get(0);
        
        for (Pessoa p : lista){
           if(p.getIdade() > pessoaMaisVelha.getIdade()){
               pessoaMaisVelha = p;
           }
       }
        return pessoaMaisVelha;
    }
    
    public static Pessoa getPessoaMaisNova(List<Pessoa> lista){
        Pessoa pessoaMaisNova = lista.get(0);
        
        for (Pessoa p : lista){
            if (p.getIdade() < pessoaMaisNova.getIdade()){
                pessoaMaisNova = p;
            }
        }
        return pessoaMaisNova;
    }
    
    
}
